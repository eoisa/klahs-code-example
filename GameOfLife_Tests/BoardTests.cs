﻿using GameOfLife;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameOfLife_Tests
{
    [TestClass]
    public class BoardTests
    {
        [TestMethod]
        public void fillInitialLifeMatrix_Test()
        {
            Board board = new Board();
            var currentState = board.getCurrentState();
            var height = board.getHeight();
            var width = board.getWidth();
            int[,] mDArray = new int[height, width];
            board.fillInitialLifeMatrix(mDArray, 0, 0);
            Assert.AreNotEqual(mDArray, currentState);
        }

        [TestMethod]
        public void updateBoard_Test()
        {
            Board board = new Board();
            var currentState = board.getCurrentState();
            board.updateBoard();
            Assert.AreNotEqual(board, currentState);
        }

        [TestMethod]
        public void EdgeWrap_Test()
        {
            int x = 5;
            int y = 5;
            Board board = new Board();
            int height = board.getHeight();
            board.EdgeWrap(ref x, ref y);
            Assert.AreNotEqual(y, height);
        }

        [TestMethod]
        public void checkRelationship_Test()
        {
            Board board = new Board();
            board.checkRelationship();
            var nextState = board.getNextState();
            var currentState = board.getCurrentState();
            Assert.AreNotEqual(nextState, currentState);
        }
    }
}
