﻿using GameOfLife;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameOfLife_Tests
{
    [TestClass]
    public class LifeTests
    {
        [TestMethod]
        public void createInitialLife_Test()
        {
            Life life = new Life();
            int[,] lifeMatrix = new int[15,15];
            life.createInitialLife();
            var postLifeMatrix = life.getLifeMatrix();
            Assert.AreNotEqual(lifeMatrix, postLifeMatrix);
        }

        [TestMethod]
        public void countNeighbours_Test()
        {
            int x = 15;
            int y = 15;
            Life life = new Life();
            Board board = new Board();
            board.checkRelationship();
            int count = life.countNeighbours(x, y);
            Assert.AreEqual(count, 0);
        }

        
    }
}
