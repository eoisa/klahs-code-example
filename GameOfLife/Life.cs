﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameOfLife
{
    public class Life
    {
        static int[,] lifeMatrix = new int[15,15];
        Board board = new Board();
        public int[,] getLifeMatrix() { return lifeMatrix; }

        public Life()
        {
            createInitialLife();         
            board.fillInitialLifeMatrix(lifeMatrix, 0, 0);
        }

        public void createInitialLife()
        {
            Random random = new Random();
            int[] intArray = new int[15];
            for (int i = 0; i < intArray.Length; i++)
            {
                intArray[i] = random.Next(0, 2);
            }

            byte[] result = new byte[intArray.Length * sizeof(int)];
            Buffer.BlockCopy(intArray, 0, result, 0, result.Length);
            Buffer.BlockCopy(result, 0, lifeMatrix, 0, 15);
        }

        public int countNeighbours(int x, int y)
        {
            int x1, y1, count = 0;
            int[,] currentState = board.getCurrentState();

            for (int sx = x - 1; sx < x + 2; sx++)
            {
                for (int sy = y - 1; sy < y + 2; sy++)
                {
                    if ((sx == x) && (sy == y))
                    {
                        continue; // don't count self
                    }
                    x1 = sx;
                    y1 = sy;
                    board.EdgeWrap(ref x1, ref y1);
                    if (currentState[x1, y1] == 1)
                    {
                        count++;
                    }
                }
            }
            return count;
        }

    }
}

            
       

