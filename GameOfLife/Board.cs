﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GameOfLife
{
    public class Board
    {
        const int HEIGHT = 50;
        const int WIDTH = 50;

        static int[,] currentState = new int[WIDTH, HEIGHT];
        static int[,] nextState = new int[WIDTH, HEIGHT];

        static StreamWriter stdout = new StreamWriter(Console.OpenStandardOutput(), System.Text.Encoding.GetEncoding(437));
        static char blob = System.Text.Encoding.GetEncoding(437).GetChars(new byte[] { 219 })[0];

        public Board()
        {
            
        }

        public int getWidth(){return WIDTH;}
        public int getHeight(){return HEIGHT;}
        public int[,] getCurrentState() { return currentState; }
        public int[,] getNextState() { return nextState; }

        public void fillInitialLifeMatrix(int[,] lifeMatrix, int xoffset, int yoffset)
        {
            currentState = getCurrentState();
            for (int y = 0; y < lifeMatrix.GetLength(0); y++)
            {
                for (int x = 0; x < lifeMatrix.GetLength(1); x++)
                {
                    currentState[x + xoffset, y + yoffset] = lifeMatrix[y, x];
                }
            }
        
        }

        public void updateBoard()
        {
            int data;
            Console.CursorTop = 0;
            Console.CursorLeft = 0;

            for (int row = 0; row < HEIGHT; ++row)
            {
                for (int col = 0; col < WIDTH; ++col)
                {
                    data = currentState[col, row];
                    if (data == 0)
                    {
                        stdout.Write(' ');
                    }
                    else if (data == 1)
                    {

                        stdout.Write(blob);
                    }
                    else
                    {
                        stdout.Write(' ');
                    }
                }
                if (row != HEIGHT - 1) stdout.Write("\n");
            }
            stdout.Flush();
        }

        public void EdgeWrap(ref int x, ref int y)
        {
            if (x < 0) x += WIDTH;
            else if (x > WIDTH - 1) x -= WIDTH;
            if (y < 0) y += HEIGHT;
            else if (y > HEIGHT - 1) y -= HEIGHT;
        }

        public void checkRelationship()
        {
            int n;
            Life life = new Life();

            for (int x = 0; x < WIDTH; x++)
            {
                for (int y = 0; y < HEIGHT; y++)
                {
                    n = life.countNeighbours(x, y);

                    if (n < 2)
                    {
                        // Die due to isolation
                        nextState[x, y] = (byte)(currentState[x, y] == 1 ? 2 : 0);
                    }
                    else if (n == 2)
                    {
                        // 2 neighbours - stable
                        nextState[x, y] = currentState[x, y];
                    }
                    else if (n == 3)
                    {
                        // 3 neighbours - new life (or same old life)
                        nextState[x, y] = 1;
                    }
                    else
                    {
                        // 4 or more - die due to overcrowding
                        nextState[x, y] = 0;
                    }
                }
            }
            // Copy new Life to the matrix
            Array.Copy(nextState, currentState, WIDTH * HEIGHT);
            Board board = new Board();
            board.updateBoard();
        }

    }
}