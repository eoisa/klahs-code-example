﻿using System;
using System.IO;

namespace GameOfLife
{
    public class RunSimulation
    {
        static void Main(string[] args)
        {  
            Board board = new Board();
            Life life = new Life();

            while(!Console.KeyAvailable)
            {
                board.checkRelationship();
                System.Threading.Thread.Sleep(50);
            }
            Console.ReadKey();
        }
    }
}
